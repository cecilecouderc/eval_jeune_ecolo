# Reporting Jeune Ecolo 

Temps consacré jour 1 : 5h (jeudi de 9h>12h30, 13h30>14h, 21h30>22h30)

## Progress
- Wireframes 
- UML : USe Case, Diag d'activités et diag de classes
- Création de la base de données via Workbench
- Création du dossier de fichiers
- Installation des modules npm (express, mysql, pug)
- Création du repo Git 
- Connexion du serveur 
- Connexion de la base de données  


## Plan
- Création des données : défis, users.. 
- Construction des templates itinérants 
- Récupération des données dans la bdd
- Layout



## Problem
- Gestion des privilèges dans une BDD  
- Gestion des défis "en cours" 
- Gestion des boolean dans une bdd 
- Serveur crash en permanence, obligée de changer de port à chaque sauvegarde. 
- Worbench et Studio Code en simultané fait planter régulièrement Workbench !! 
- 

