//config
const session = require('express-session');
const bodyParser = require('body-parser');
const express = require('express');
const mysql = require('mysql');
require('dotenv').config();
const app = express();
const path = require('path');

const connection = mysql.createConnection({ //connection BDD
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE
})

connection.connect((err) => {
    if (err) {
        console.log('Error, try again')
    }
    console.log('Connected to DataBase !')
})

//utilisation des modules
app.use(express.static(`${__dirname}/public`));
app.use(session({
    secret: 'secret'
}));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.set('view engine', 'pug');

/// registration

app.get('/connexion/register', (req, res) => res.render('connexion/register'));

app.post('/register/store', (req, res) => {
    connection.query(`SELECT * FROM Users WHERE pseudo='req.body.pseudo'`, (err, result, fields) => {
        if (result.length < 1) {
            connection.query(`INSERT INTO Users (pseudo, email, password) VALUES ('${req.body.pseudo}', '${req.body.email}', '${req.body.password}')`)
            res.redirect('/');
        }
        else {
            res.send('Ce pseudo est déjà utilisé !')
        }
    })
})

//connexion 

app.get('/', (req, res) => res.render('connexion/connect'));



app.post("/store", (req, res) => {
    connection.query(`SELECT * FROM Users WHERE pseudo='${req.body.pseudo}'`, (err, result, fields) => {
        console.log(result);
        if (result.length < 1) {
            res.send("ce pseudo n'existe pas !");
        }
        else if (result[0].password == req.body.password) {
            req.session.pseudo = req.body.pseudo;
            // res.send(`Bonjour ${req.session.pseudo}`);
            res.redirect('/posts/index');
        }
        else {
            res.send("Ce n'est pas le bon mot de passe !")
        }
    })
})

app.get('/posts/index', (req, res) =>  {
    connection.query(`SELECT title FROM Challenges`, 
    (err, result) => {
        if (err) {
            console.log(err);
            return;
        }
        console.log(result);
        res.render('posts/index', {results: result});
    });
});
//creation challenges
app.post('/challenges/store', (req, res) => {
    console.log(req.body)
    connection.query(`INSERT INTO Challenges (title, content, impact) VALUES (
        '${req.body.title}', '${req.body.content}', '${req.body.impact}');`, (err, result, fields) => {
        if (err) {
            console.log(err);
        }
    });
    res.redirect('/challenges/create');
});
//affichage creation challenges / admin 

app.get('/challenges/create', (req, res) => res.render('challenges/create'))
app.post('')


// //affichage challenges pour users
// app.get('/challenges', (req, res) => {
//     connection.query(`SELECT * FROM Challenges;`, (err, result, fields) => { // mise en place des requêtes pour les défis 
//         if (err) {
//             console.log(err);
//             return;
//         }
//         res.render('challenges/index', {results: result});
//         console.log(result);
//     });
// })

// //creation advises
// // app.post('/advises/store', (req, res) => {
// //     console.log(req.body) 
// //     connection.query(`INSERT INTO Advises (content) VALUES (
// //         '${req.body.content}');`, (err, result, fields) => {
// //         if (err) {
// //             console.log(err);
// //         }
// //     }); 
// //     res.redirect('/challenges');
// // });

// //affichage advises 
// app.get('/advises', (req, res) => {
//     connection.query(`SELECT * FROM Advises;`, (err, result, fields) => { 
//         if (err) {
//             console.log(err);
//             return;
//         }
//         res.render('challenges/index', {results: result});
//     });
// });
app.listen(process.env.PORT, () => console.log(`Listen on ${process.env.PORT}`));